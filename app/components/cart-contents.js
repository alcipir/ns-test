import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  cart: service('shopping-cart'),
  actions: {
    removeItem: function(item) {
      this.get('cart').remove(item);
    },

    buyCart: function(cart) {
      this.get('cart').empty();
    }
  }
});

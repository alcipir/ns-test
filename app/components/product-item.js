import Component from '@ember/component';
import { inject as service } from '@ember/service'

export default Component.extend({
  tagName: 'div',
  cart: service('shopping-cart'),

  actions: {
    add(item) {
      this.get('cart').add(item);
    }
  }
});

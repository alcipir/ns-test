import Ember from 'ember';
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  cart: service('shopping-cart'),
  actions: {
    willTransition: function() {
      this.get('cart').set('bought', false);
    }
  },

  model() {
    const items = Ember.$.getJSON("data/products.json");
    return items;
  }
});

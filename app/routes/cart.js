import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  cart: service('shopping-cart'),
  bought() {
    return this.get('cart').get('bought');
  }
});

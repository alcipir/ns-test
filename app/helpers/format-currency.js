import { helper } from '@ember/component/helper';

export function formatCurrency(params) {
  let [format, price] = params;
  price = String(price);
  price = price.replace('.',',');
  return format + " " + price + 0;
}

export default helper(formatCurrency);

import Ember from 'ember';
import Service from '@ember/service';

export default Service.extend({
  items: null,
  totals: 0,
  bought: false,
  session: window.sessionStorage,
  isEmpty: function() {
    return this.get('totals') == 0 && this.get('bought') == false;
  }.property('totals'),

  init() {
    this._super(...arguments);
    if (this.get('session').getItem('items') !== undefined &&
        this.get('session').getItem('items') !== null
       ) {
      this.set('items', JSON.parse(this.get('session').getItem('items')));
      let totals = 0;
      this.get('items').forEach(function(cartItem) {
        totals += cartItem.qty;
      });
      this.set('totals', totals);
    } else {
      this.set('items', []);
    }
  },

  add(item) {
    let found = false;
    this.get('items').forEach(function(cartItem) {
      if (item.id == cartItem.id) {
        found = true;
        Ember.set(cartItem, "qty", cartItem.qty + 1);
      }
    });

    if (!found) {
      Ember.set(item, "qty", 1);
      this.get('items').pushObject(item);
    }
    let totals = this.get('totals') + 1;
    this.set('totals', totals);
    this.get('session').setItem('items', JSON.stringify(this.get('items')));
  },

  remove(item) {
    this.get('items').removeObject(item);
    let totals = this.get('totals') - item.qty;
    this.set('totals', totals);
    this.get('session').setItem('items', JSON.stringify(this.get('items')));
  },

  empty() {
    this.set('totals', 0);
    this.get('items').clear();
    this.set('bought', true);
    this.get('session').setItem('items', JSON.stringify(this.get('items')));
  }
});
